# Porsupuester (Wordpress)

UX research about budget APP "Porsupuester" to help freelancer through a mobile app

## Index

- [1. Introduction](#1-introduction)
- [2. Team](#2-team)
- [3. Strategy](#3-strategy)
- [4. Solutions Scope](#4-solutions-scope)
- [5. Benchmark](#5-benchmark)
- [6. Customer Journey Map](#6-customer-journey-map)
- [7. Navigation](#7-navigation)
- [8. Wireframes](#8-wireframes)
- [9. Mockups](#9-mockups)
- [10. Accessibility](#10-accessibility)




---

## 1. Introduction

Many businesses have the need to accurately and efficiently deliver budgets to their clients, with detailed information about the services they are offering and their prices and even estimated man-hours. This solution comes from the fact that these budgets are often being manually made and take a lot of time and resources from other more important activities.

With this project we want to make an efficient and accurate budgeting tools, to help freelancers, specifically in the WordPress implementation services industry.

---

## 2. Team

Belfor Acuña - Researcher

Marco Caceres - Designer

Carlo Negroni - Designer

Germán Jara - Researcher

---

## 3. Strategy

The initial action carried out by the team involves defining the strategy. They employ a "Value Proposition Canvas" to evaluate how Porsupuester aligns with the customer's requirements and the potential solution.

![Value Proposition Canvas](./Files/Value_Canvas.png)

---

## 4. Solutions Scope

Once a product strategy has been established, it becomes crucial to delineate the scope. In this phase, we construct a profile of our ideal customer and envision how our product will cater to their needs.

![Person_UX_1](./Files/Personas/Persona_1.png)

![Person_UX_2](./Files/Personas/Persona_2.png)

![Person_UX_3](./Files/Personas/Persona_3.png)

---

## 5. Benchmark

Benchmarking lets us leverage competitor strengths, avoid their weaknesses, and design an application that aligns with market expectations and delivers an exceptional user experience.

![Benchmarking](./Files/Benchmark.png)

---

## 6. Customer Journey Map

The Customer Journey Map helps understand the user experience, identifying emotions and needs at each stage, to enhance the experience and strengthen the relationship with users.

![Customer_Journey_Map](./Files/Customer_Journey_Map.png)

---

## 7. Navigation

![Navigation](./Files/Navigation_1.png)

---

## 8. Wireframes

Wireframes are essential in design and development for several reasons. They provide a visual representation that helps plan and visualize the structure and layout of a product or website before investing significant resources. They serve as an efficient communication tool, facilitating discussions and aligning expectations among team members and stakeholders. Also support an iterative design process, allowing for quick experimentation and problem-solving while being cost-effective compared to high-fidelity prototypes. Lastly, wireframes promote user-centered design by prioritizing user needs and goals, resulting in more intuitive and efficient interfaces.

![Home](./Files/Wireframe_V1.png)

---

## 9. Mockups

This section shows the mockups developed as a solution, for a better visualization visit the project in figma together with its functionalities [here](https://www.figma.com/file/n636Oy1bCPUY6rC5EHDPSX/Presupuestador-mockups?type=design&node-id=1%3A2&mode=design&t=anMToQSZxBhUavrv-1).

## Versión 1 Mockups Porsupuester

![Mock1](./Files/Mockup_V1.png)

## Versión 2 Mockups Porsupuester

![Mock2](./Files/Mockup_v2(1).png)
![Mock3](./Files/Mockup_v2(2).png)
![Mock4](./Files/Mockup_v2(3).png)


## Versión 3 Mockups Porsupuester

![Mock5](./Files/Mockup_V3(1).png)
![Mock6](./Files/Mockup_V3(2).png)
![Mock7](./Files/Mockup_V3(3).png)
![Mock8](./Files/Mockup_V3(4).png)

## 10. Accessibility
Accessibility menu with the option to activate zoom, increase and reduce the font size

![Accessibility1](./Files/Accessibility(1).png)


Dark mode and an option to change the language

![Accessibility2](./Files/Accessibility(2).png)